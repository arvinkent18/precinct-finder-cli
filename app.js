const fs = require("fs");
const inquirer = require("inquirer");
const chalk = require("chalk");
const figlet = require("figlet");

// Initialize the header for the application along with the author's name
const init = () => {
  console.log(
    chalk.green(
      figlet.textSync("COMELEC PRECINCT FINDER", {
        font: "contessa",
        horizontalLayout: "default",
        verticalLayout: "default"
      })
    )
  );
  console.log(
    chalk.green(
      figlet.textSync("Developed by Arvin Kent S. Lazaga of AdZU CITS", {
        font: "contessa",
        horizontalLayout: "default",
        verticalLayout: "default"
      })
    )
  );
};

// Initialize the default questions for the user to interact
const askQuestions = () => {
  const questions = [
    {
      name: "NAME",
      type: "input",
      message: "Enter Voter's Name(Lastname Firstname Middlename): "
    }
  ];

  return inquirer.prompt(questions);
};

// Asks the user to either repeat the search again
const askRepeat = () => {
  const question = [
    {
      name: "REPEAT",
      type: "input",
      message: "Type Y to search again or N to exit?"
    }
  ];

  return inquirer.prompt(question);
};

// Show the repeat question
const showRepeatQuestion = async () => {
  const answer = await askRepeat();
  const { REPEAT } = answer;

  getRepeatAnswer(REPEAT);
};

// Gets the answer from the user if he or she will repeat the search
const getRepeatAnswer = userAnswer => {
  if (userAnswer == "y" || userAnswer == "Y") {
    run(false);
  } else {
    console.log(
      chalk.white.bgGreen.bold("Thank you for using the application!")
    );
    process.exit();
  }
};

// Read and Opens every Barangay JSON files in the directory
//
// Gets the Precinct number and other information of the voter
//
// @params name
const getVoterPrecinct = name => {
  const votersDirectory = __dirname + "/data/";
  let found = false;

  fs.readdir(votersDirectory, (err, barangayFiles) => {
    if (err) {
      console.log(chalk.white.bgRed.bold("Could not find directory", err));
      process.exit(1);
    } else {
      console.log(
        chalk.white.bgBlue.bold(
          "Searching voter through all Barangays, please wait...."
        )
      );

      fs.readFile(votersDirectory + 'comelec.json', (err, data) => {
        if (err) {
          console.log(chalk.white.bgRed.bold(err));
        }

        console.log(
          chalk.white.bgBlue.bold(
            `\nCurrently searching voter....`
          )
        );

        const votersList = JSON.parse(data);
        let no = "";
        let vName = "";
        let precinct = "";
        let address = "";
              
        votersList.some(voter => {
          if (voter.name == name.toUpperCase()) {
            found = true;
            vName = voter.name;
            no = voter.no;
            precinct = voter.precinct;
            address = voter.address;

            return true;
          } else {
            found = false;

            return false;
          }
        });

        if (found == true) {
          console.log(
            chalk.white.bgGreen.bold(`Voter found at Barangay ${address}!`)
          );
          console.log(
            chalk.white.bgGreen.bold(
              `Details:\nName: ${vName}\nNo: ${no}\nPrecinct: ${precinct}\nAddress: ${address}`
            )
          );
          showRepeatQuestion();
        } else {
          console.log(
            chalk.white.bgRed.bold(
              `\nVoter not found!`
            )
          );
          showRepeatQuestion();
        }
      });
    }
  });
};

// Initializes the application with the main questions
//
// @params header
const run = async (header = true) => {
  if (header != false) {
    init();
  }

  const answers = await askQuestions();
  const { NAME } = answers;

  getVoterPrecinct(NAME);
};

// Runs the application
run();
